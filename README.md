json-reader
===========

After finding that JSMN <https://bitbucket.org/zserge/jsmn> was using too much
memory for storing the description tokens, I wrote this alternative JSON reader
that trades lower memory usage for higher CPU usage. Memory usage is a copy of 
the path searched for plus stack memory based on amount of nesting of JSON data.

Features
--------

* C99; no dependencies except standard library:
    * strncmp, strlen, strcspn, memcpy, memmove, alloca
    * optional: strcmp, malloc, realloc, free
* Parses JSON structures in UTF-8 strings using a recursive method using only stack memory
* Easy to find and extract values using search paths
* Optional memoization to allow trading heap memory usage for faster lookups
* Uses about 3k of Flash on an ARM7 device (Thumb mode)
* Handles *most* JSON data types: 
    * Objects
    * Arrays
    * Booleans
    * Null
    * For numbers only integers are currently handled, but adding floats is easy
    * Handles "basic" strings, see below

Usage
-----

Basic usage, ignoring error checking:

    json_token_t json;
    json_validate(json_string, &json);
    
    json_token_t value;
    json_find(&json, "key1/subkey:2/value1", &value);
    
    char s[bufsize];
    json_decode_string(&value, s, bufsize);
    
    printf("%s\n", s);
    
    json_free(&json);

* The first step checks that the string is a valid JSON string, and 
gives a starting point for further calls. If using memoization, 
this step will also populate the cache.

* Next we locate a value in the structure using a path.

* Finally we extract a string and print it.

* Release any memory used (only needed for token that we validated into)

Let's assume the JSON data looks like this:

    {
        "key1": {
            "some key": "ignored value",
            "subkey": [
                {...}, 
                {...}, 
                {
                    "value1": "target string"
                }
            ]
        }
    }

Note the path argument to the json_find() function: we ask it to
locate "key1" in the containing JSON object, assume that this is 
a sub object that contains an key "subkey" which is an array, 
extract the third item in the array and finally find the key 
named "value1" in this object.

You can also iterate over arrays, in case you don't know the size in advance.

Mis-features / future work
--------------------------

* For numbers only integers are currently handled, not floats/doubles
* Handles basic strings, plus *some* encoding details; e.g. backslash 
  escaped values. Only Unicode values 0-255 are allowed at the moment.
  Support for wide chars and extracting raw UTF8 is for the Future .
* Could use more functionality for exploring unknown JSON structures
* More sanity checking of input data would be nice. Basically, use this 
  with "friendly" JSON data; expect it to be unable to handle user-supplied 
  data in the general case. At worst it should return an error, though, 
  and not crash.

License
-------

Copyright (c) 2012-2015, TLab West Systems AB

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in thedocumentation and/or other materials provided with the distribution.
* Neither the name of Tlab West Systems AB nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
    
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TLAB WEST SYSTEMS AB BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

