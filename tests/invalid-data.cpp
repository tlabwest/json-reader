#include "helpers.h"


TEST(InvalidData, UnendingString) {
	std::string json = read_file("samples/unending-string.json");
	
	json_token_t token;
	
	json_err_t res = json_validate(json.c_str(), &token);
	
	ASSERT_EQ(res, json_err_invalid);
}


TEST(InvalidData, UnendingNumber) {
	std::string json = read_file("samples/unending-number.json");
	
	json_token_t token;
	
	json_err_t res = json_validate(json.c_str(), &token);
	
	ASSERT_EQ(res, json_err_invalid);
}


TEST(InvalidData, UnendingArray1) {
	std::string json = read_file("samples/unending-array-1.json");

	json_token_t token;

	json_err_t res = json_validate(json.c_str(), &token);

	ASSERT_EQ(res, json_err_invalid);
}


TEST(InvalidData, UnendingArray2) {
	std::string json = read_file("samples/unending-array-2.json");

	json_token_t token;

	json_err_t res = json_validate(json.c_str(), &token);

	ASSERT_EQ(res, json_err_invalid);
}

