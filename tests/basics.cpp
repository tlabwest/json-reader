#include "helpers.h"


TEST(Basics, ParseMinimal) {
	std::string json = read_file("samples/minimal.json");
	
	json_token_t token;
	
	json_err_t res = json_validate(json.c_str(), &token);
	
	ASSERT_EQ(res, json_err_ok);
	
	json_free(&token);
}


TEST(Basics, ParseSimple) {
	std::string json = read_file("samples/simple.json");
	
	json_token_t token;
	
	json_err_t res = json_validate(json.c_str(), &token);
	
	ASSERT_EQ(res, json_err_ok);
	
	json_free(&token);
}


TEST(Basics, ParseComplex) {
	std::string json = read_file("samples/complex.json");
	
	json_token_t token;
	
	json_err_t res = json_validate(json.c_str(), &token);
	
	ASSERT_EQ(res, json_err_ok);
	
	json_free(&token);
}

