#ifndef HELPERS_H
#define HELPERS_H

#include "gtest/gtest.h"

#include <string>
#include <iostream>
#include <fstream>

#include "../src/json-reader.h"

std::string read_file(std::string path);


#endif

