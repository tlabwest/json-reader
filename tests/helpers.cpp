#include "helpers.h"


std::string read_file(std::string path)
{
	std::ifstream f;
	f.open(path.c_str());
	std::string res;
	
	if ( !f.is_open() )
	{
		return "";
	}
	
	while ( !f.eof() )
	{
		std::string part;
		f >> part;
		res += part;
	}
	
	f.close();
	
	return res;
}

