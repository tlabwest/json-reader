//
//  json-reader.c
//  json-reader
//
//  Copyright (c) 2012-2014 TLab West Systems AB. All rights reserved.
//  See LICENSE.txt for details.
//


#include "json-reader.h"

#include <stdlib.h>
#include <stdint.h>
#include <string.h>


#if JSON_MEMOIZE

#include <stdio.h>

// only used internally
typedef struct json_memo_t json_memo_t;

// only used internally
struct json_memo_t {
	json_token_t value;
};

// only used internally
struct json_memo_list_t {
	//	int max_memo_level;
	size_t num_memos;
	json_memo_t *memos;
};


static inline size_t memo_find_index(json_memo_list_t *list, size_t start)
{
    size_t index = 0;
    
    while (index < list->num_memos && list->memos[index].value.start < start )
    {
        index++;
    }
    
    return index;
}


static json_memo_t* memo_find(json_memo_list_t *list, size_t start)
{
	size_t index = memo_find_index(list, start);
	
	if ( index < list->num_memos && list->memos[index].value.start == start )
	{
		return &list->memos[index];
	}
	
	return NULL;
}


static void memo_store(json_memo_list_t *list, json_token_t *value)
{
	if ( value->level > MEMOIZE_MAX_LEVELS || value->type == json_type_invalid)
	{
		return;
	}
	
	size_t previous_size = list->num_memos;
	
	size_t index = memo_find_index(list, value->start);
	
	list->num_memos++;
	
	list->memos = realloc(list->memos, list->num_memos * sizeof(json_memo_t));
	
	if ( index < previous_size - 1 )
	{ // insert in middle
		memmove(&list->memos[index+1], &list->memos[index], sizeof(json_memo_t)*(previous_size - index));
	}
    
    memcpy(&list->memos[index].value, value, sizeof(json_token_t));
}

#endif // JSON_MEMOIZE


static json_err_t read_value(json_token_t *parser, json_token_t *token);


static inline bool digit(char c)
{
	return c >= '0' && c <= '9';
}


static inline bool hexdigit(char c)
{
	return digit(c) || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}


static inline char next(json_token_t *parser)
{
	return parser->source[parser->_pos];
}


/*
 Returns false if we jump off the end of the source data
 */
static inline bool skip(json_token_t *parser)
{
	parser->_pos++;
	
	if ( parser->_pos > parser->end )
	{
        return false;
	}
    
    return true;
}


/*
 Returns false if we jump off the end of the source data
*/
static inline bool skip_whitespace(json_token_t *parser)
{
    while (parser->_pos < parser->end)
    {
        switch ( next(parser) )
        {
			case ' ':
			case '\t':
			case '\n':
			case '\r':
				if ( !skip(parser) )
                {
                    return false;
                }
                break;
				
            default:
                return true;
        }
    }
    
    return false;
}


static inline json_type_t next_type(json_token_t *parser)
{
	json_type_t res;
	
	switch ( next(parser))
	{
		case '{':
			res = json_type_object;
			break;
			
		case '"':
			res = json_type_string;
			break;
			
		case '-':
		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9':
			res = json_type_number;
			break;
			
		case '[':
			res = json_type_array;
			break;
			
		case 't':
		case 'f':
			res = json_type_bool;
			break;
			
		case 'n':
			res = json_type_null;
			break;
			
		default:
			res = json_type_invalid;
			break;
	}
	
	return res;
}


static inline json_err_t read_string(json_token_t *parser, json_token_t *value)
{
	value->start = parser->_pos + 1;
	
	bool escaped = false;
	
	for (;;)
	{
		if ( !skip(parser) )
		{
			return json_err_invalid;
		}
		
		if ( escaped )
		{
			switch (next(parser))
			{
				case '"':
				case '\\':
				case '/':
				case 'b':
				case 'f':
				case 'n':
				case 'r':
				case 't':
					break;
                    
				case 'u':
					// read four hex digits
                    for ( int i=0; i<4; ++i )
                    {
                        if ( !skip(parser))
                        {
                            return json_err_invalid;
                        }
                        
                        if ( !hexdigit(next(parser)))
                        {
                            return json_err_invalid;
                        }
                    }
					break;
					
				default:
					return json_err_invalid;
			}
			
			escaped = false;
			
			continue;
		}
		
		/*
		 if ( next(parser) < 20 )
		 { // control chars not allowed
		 return json_err_invalid;
		 }
		 */
		
		switch ( next(parser) )
		{
			case '\\':
				escaped = true;
				continue;
				
			case '"':
			{ // end of string
				value->end = parser->_pos;
				value->type = json_type_string;
				value->size = value->end - value->start;
				value->type = json_type_string;
				
                if ( !skip(parser) )
                {
                    return json_err_invalid;
                }
			}	goto done;
		}
	}
	
done:
	
	return json_err_ok;
}


static json_err_t read_number(json_token_t *parser, json_token_t *value)
{
	value->start = parser->_pos;
	
	if ( next(parser) == '-' )
	{
        if ( !skip(parser) )
        {
            return json_err_invalid;
        }
	}
	
	while ( digit(next(parser)) )
	{
        if ( !skip(parser) )
        {
            return json_err_invalid;
        }
	}
	
	value->end = parser->_pos;
	value->size = value->end - value->start;
	value->type = json_type_number;
	
	return json_err_ok;
}


static json_err_t read_bool(json_token_t *parser, json_token_t *value)
{
	value->start = parser->_pos;
	
	switch ( next(parser) )
	{
		case 't':
            if ( parser->_pos >= parser->end - 4 )
            {
                return json_err_invalid;
            }
            
			if ( strncmp("true", &parser->source[parser->_pos], 4) == 0 )
			{
				parser->_pos += 4;
			}
			break;
			
		case 'f':
            if ( parser->_pos >= parser->end - 5 )
            {
                return json_err_invalid;
            }
            
			if ( strncmp("false", &parser->source[parser->_pos], 5) == 0 )
			{
				parser->_pos += 5;
			}
			break;
			
		default:
			return json_err_invalid;
	}
	
	if ( !skip_whitespace(parser) )
    {
        return json_err_invalid;
    }
    
	switch ( next(parser) )
	{
		case ',':
		case ']':
		case '}':
			break;
			
		default:
			return json_err_invalid;
	}
	
	value->end = parser->_pos;
	value->size = value->end - value->start;
	value->type = json_type_bool;
	
	return json_err_ok;
}


static json_err_t read_null(json_token_t *parser, json_token_t *value)
{
	value->start = parser->_pos;
	
    if ( parser->_pos >= parser->end - 4 )
    {
        return json_err_invalid;
    }
    
	if ( strncmp("null", &parser->source[parser->_pos], 4) == 0 )
	{
		parser->_pos += 4;
	}
	
	if ( !skip_whitespace(parser) )
    {
        return json_err_invalid;
    }
    
	switch ( next(parser) )
	{
		case ',':
		case ']':
		case '}':
			break;
			
		default:
			return json_err_invalid;
	}
	
	value->end = parser->_pos;
	value->size = value->end  - value->start;
	value->type = json_type_null;
	
	return json_err_ok;
}


static json_err_t read_array(json_token_t *parser, int target_offset, json_token_t *value)
{
	if ( target_offset >= 0 )
	{
		parser->_pos = parser->start;
	} else
	{
		value->start = parser->_pos;
	}
	
	int num_items = 0;
	json_err_t res = json_err_invalid;
	
	for (;;)
	{
        // note that both these calls (potentially) move us forward
		if ( !skip(parser) || !skip_whitespace(parser) )
        {
            return json_err_invalid;
        }
        
		if ( next(parser) == ']' )
		{ // empty list
			res = target_offset < 0 ? json_err_ok : json_err_not_found;
			skip(parser); // we can ignore the return value here, we know it's true
			break;
		}
		
		res = read_value(parser, target_offset == num_items ? value : NULL);
		
		if ( res != json_err_ok || target_offset == num_items )
		{
			goto done;
		}
		
		num_items++;
		
		if ( !skip_whitespace(parser) )
        {
            return json_err_invalid;
        }
        
		switch ( next(parser) )
		{
			case ',':
				continue;
				
			case ']':
				res = target_offset >= 0 ? json_err_not_found : json_err_ok;
				skip(parser); // we can ignore the return value here, we know it's true
				goto done;
				
			default:
				return json_err_invalid;
		}
	}
	
done:
    if ( target_offset < 0 && res == json_err_ok )
    {
		value->type = json_type_array;
		value->end = parser->_pos;
        value->size = (size_t) num_items;
#if JSON_MEMOIZE
        value->lookup = parser->lookup;
#endif
    }
	
	return res;
}


static json_err_t read_object(json_token_t *parser, const char *target_name, json_token_t *token)
{
	if ( target_name )
	{
		parser->_pos = parser->start;
	}
	
	token->start = parser->_pos;
	
	for(;;)
	{
		json_err_t res;
		
		// note that both these calls can move us ahead
        if ( !skip(parser) || !skip_whitespace(parser) )
        {
            return json_err_invalid;
        }
        
		if ( next(parser) == '}' )
		{
			goto done;
		}
		
		// read name
		json_token_t name_token;
		
		res = read_value(parser, &name_token);
		
		if ( res != json_err_ok || name_token.type != json_type_string )
		{
			return json_err_invalid;
		}
		
		if ( !skip_whitespace(parser) )
        {
            return json_err_invalid;
        }
        
		if ( next(parser) != ':' )
		{
			return json_err_invalid;
		}
		
		if ( !skip(parser) )
        {
            return json_err_invalid;
        }
		
		// read value
		res = read_value(parser, target_name ? token : NULL);
		
		if ( res != json_err_ok )
		{
			return res;
		}
		
		// check if we should store this in a lookup table
		
		if ( target_name )
		{ // check if this was what we're looking for
			size_t token_len = name_token.end - name_token.start;
			
			if ( strncmp(target_name, &parser->source[name_token.start], token_len) == 0 && strlen(target_name) == token_len )
			{
                return json_err_ok;
			}
		}
		
		if ( !skip_whitespace(parser) )
        {
            return json_err_invalid;
        }
        
		switch ( next(parser) )
		{
			case '}':
				goto done;
				
			case ',':
				continue;
				
			default:
				return json_err_invalid;
		}
	}
	
done:
	if ( !skip(parser) )
    {
        return json_err_invalid;
    }
	
	if ( target_name )
	{
		return json_err_not_found;
	}
	
	token->end = parser->_pos;
	token->size = token->end - token->start;
	token->type = json_type_object;
#if JSON_MEMOIZE
    token->lookup = parser->lookup;
#endif
	
	return json_err_ok;
}


static json_err_t read_value(json_token_t *parser, json_token_t *token)
{
	if ( !skip_whitespace(parser) )
    {
        return json_err_invalid;
    }
    
#if JSON_MEMOIZE
	// check memoization
	if ( parser->level <= MEMOIZE_MAX_LEVELS )
	{
		json_memo_t *memo = memo_find(parser->lookup, parser->_pos);
		
		if ( memo )
		{
			parser->_pos = memo->value.end;
			
			if ( token )
			{
				memcpy(token, &memo->value, sizeof(json_token_t));
			}
			
			return json_err_ok;
		}
	}
#endif
	
	json_err_t res = json_err_invalid;
	
#if JSON_MEMOIZE
	parser->level++;
#endif
    
	json_token_t value;
	value.source = parser->source;
	value.type = json_type_invalid;
#if JSON_MEMOIZE
	value.level = parser->level;
#endif
	
	switch ( next_type(parser) )
	{
		case json_type_object:
			res = read_object(parser, NULL, &value);
			break;
			
		case json_type_array:
			res = read_array(parser, -1, &value);
			break;
			
		case json_type_string:
			res = read_string(parser, &value);
			break;
			
		case json_type_number:
			res = read_number(parser, &value);
			break;
			
		case json_type_bool:
			res = read_bool(parser, &value);
			break;
			
		case json_type_null:
			res = read_null(parser, &value);
			break;
			
		case json_type_invalid:
			res = json_err_invalid;
			break;
	}
	
#if JSON_MEMOIZE
	// store memoization
	if ( res == json_err_ok )
	{
		memo_store(parser->lookup, &value);
	}
    
	parser->level--;
#endif
	
	if ( token )
	{
		memcpy(token, &value, sizeof(json_token_t));
#if JSON_MEMOIZE
		token->lookup = parser->lookup;
#endif
	}
	
	return res;
}


json_err_t json_validate(const char *str, json_token_t *token)
{
	json_token_t parser;
	parser.source = str;
	parser._pos = 0;
	parser.start = 0;
	parser.end = strlen(str);
	parser.size = parser.end;
#if JSON_MEMOIZE
	parser.lookup = malloc(sizeof(json_memo_list_t));
	if ( parser.lookup == NULL )
	{
		return json_err_nomem;
	}
	parser.lookup->memos = NULL;
	parser.lookup->num_memos = 0;
	parser.level = 0;
#endif
	
	token->type = json_type_invalid;
	token->source = str;
#if JSON_MEMOIZE
	token->lookup = parser.lookup;
#endif
	
	if ( !skip_whitespace(&parser) )
	{
		return json_err_invalid;
	}
	
	if ( next_type(&parser) != json_type_object )
	{
		return json_err_invalid;
	}
	
	json_err_t res = read_object(&parser, NULL, token);
	
#if JSON_MEMOIZE
	token->level = parser.level;
	if ( token->level != 0 )
	{
		res = json_err_invalid;
	}
#endif
	
	return res;
}


void json_free(json_token_t *token)
{
#if JSON_MEMOIZE
	if ( token->lookup->memos )
	{
		free(token->lookup->memos);
        token->lookup->memos = NULL;
	}
	
	free(token->lookup);
    token->lookup = NULL;
#else
	(void)token;
#endif
}


void json_array_prepare(json_token_t *array)
{
    array->_pos = array->start + 1 /* start after initial [ */;
}


json_err_t json_array_next(json_token_t *array, json_token_t *value)
{
    value->type = json_type_invalid;
    
    if ( array->_pos >= array->end )
    {
        return json_err_not_found;
    }
    
    json_err_t res = read_value(array, value);
    
    // skip over whitespace and ',' to next item
    if ( !skip_whitespace(array) || !skip(array) )
    {
        return json_err_invalid;
    }
    
    return res;
}


json_err_t json_find(const json_token_t *object, const char *_path, json_token_t *value)
{
	value->type = json_type_invalid;
	
	if ( object->type == json_type_invalid )
	{
		return json_err_invalid;
	}
	
	json_err_t res = json_err_invalid;
	
    char *path_copy = alloca(strlen(_path)+1);
    strcpy(path_copy, _path);
    
    char *path = path_copy;
	
	bool name_element = true;
	
	json_token_t current;
	memcpy(&current, object, sizeof(json_token_t));
	
    while ( *path )
    {
        size_t separator = strcspn(path, "/:");
		
		char separator_char = path[separator];
		
		path[separator] = 0;
		
		if ( name_element )
		{ // find value by name
			res = read_object(&current, path, value);
		} else
		{ // find value at index
			res = read_array(&current, atoi(path), value);
		}
		
		if ( res != json_err_ok )
		{
			value->type = json_type_invalid;
			goto done;
		}
		
		memcpy(&current, value, sizeof(json_token_t));
		
		switch ( separator_char )
		{
			case '/':
				name_element = true;
				path = &path[separator+1];
				break;
				
			case ':':
				name_element = false;
				path = &path[separator+1];
				break;
				
			case '\0':
				path = &path[separator];
				break;
		}
    }
    
#if JSON_MEMOIZE
	value->lookup = object->lookup;
#endif
	
	res = json_err_ok;
	
done:
	// free(path_copy); not needed when using alloca()
	
    return res;
}


json_err_t json_decode_string(json_token_t *token, char *buffer, size_t bufsize)
{
    if ( token->type != json_type_string )
    {
        return json_err_invalid;
    }
	
    size_t bufpos = 0;
	
    buffer[bufsize-1] = 0;
    
	token->_pos = token->start;
	
    /* Skip starting quote */
    for (;token->_pos < token->end; skip(token))
    {
        char c = next(token);
		
        /* Backslash: Quoted symbol expected */
        if (c == '\\')
        {
			if ( !skip(token) )
            {
                return json_err_invalid;
            }
			
            switch (next(token))
            {
                    /* Allowed escaped symbols */
                case '\"':
                    buffer[bufpos++] = '\"';
                    break;
                case '/' :
                    buffer[bufpos++] = '/';
                    break;
                case '\\' :
                    buffer[bufpos++] = '\\';
                    break;
                case 'b' :
                    buffer[bufpos++] = '\b';
                    break;
                case 'f' :
                    buffer[bufpos++] = '\f';
                    break;
                case 'r' :
                    buffer[bufpos++] = '\r';
                    break;
                case 'n'  :
                    buffer[bufpos++] = '\n';
                    break;
                case 't' :
                    buffer[bufpos++] = '\t';
                    break;
                    /* Allows escaped symbol \uXXXX */
                case 'u':
                {
                    uint16_t value = 0;
					
					if ( token->_pos + 4 > token->end )
					{
						return json_err_invalid;
					}
					
                    for ( int i = 0; i < 4; ++i )
                    {
						c = next(token);
						
                        if ( c >= '0' && c <= '9')
                        {
                            value = (uint16_t)((value << 4) + (c - '0'));
                        }
                        else if ( c >= 'A' && c <= 'F')
                        {
                            value = (uint16_t)((value << 4) + (c - 'A' + 10));
                        }
                        else if ( c >= 'a' && c <= 'f')
                        {
                            value = (uint16_t)((value << 4) + (c - 'a' + 10));
                        }
						
                        if ( !skip(token) )
                        {
                            return json_err_invalid;
                        }
                    }
					
                    if ( value == 0 || value > 0xFF )
                    {
                        // FUTURE add suport for wide chars
                        return json_err_invalid;
                    }
					
                    buffer[bufpos++] = (char)value;
                }
					break;
					
                default:
					/* Unexpected symbol */
                    return json_err_invalid;
            }
			
			continue;
        }
		
		if ( c & 0x80 )
		{
			// decode utf8
			int additional_bytes = 0;
			unsigned int value = 0;
			
			if ( (c & 0xE0) == 0xC0 )
			{
				// 1 byte more
				additional_bytes = 1;
				value = c & 0x1F;
			}
			else if ( (c & 0xF0) == 0xE0 )
			{
				// 2 bytes more
				additional_bytes = 2;
				value = c & 0x0F;
			}
			else if ( (c & 0xF8) == 0xF8 )
			{
				// 3 bytes more - unsupported
				// FUTURE add support for Unicodes > 0xFFFF
				return json_err_invalid;
			}
			else
			{
				// invalid start of utf8 sequence
				return json_err_invalid;
			}
			
			for ( int i = 0; i < additional_bytes; ++i )
			{
				if ( !skip(token) )
                {
                    return json_err_invalid;
                }
				
				c = next(token);
				
				if ( (c & 0xC0) != 0x80 )
				{
					return json_err_invalid;
				}
				
                // TODO make sure non-last bytes contain at least
                // one non-zero bit
                
				value = (value << 6) | (c & 0x3F);
			}
			
			if ( value > 0xFF )
			{
				// FUTURE support wide chars
				return json_err_invalid;
			}
			
			buffer[bufpos++] = (char)value;
			
			continue;
		}
		else
		{
			buffer[bufpos++] = c;
		}
		
        if ( bufpos == bufsize )
        {
            buffer[bufsize-1] = 0;
            return json_err_nomem;
        }
    }
	
    buffer[bufpos++] = 0;
	
    return json_err_ok;
}


json_err_t json_decode_int(json_token_t *value, int *target, int default_value)
{
	*target = default_value;
	
	if ( value->type != json_type_number )
	{
		return json_err_invalid;
	}
	
	int v = 0;
	
	bool negative = false;
	
	value->_pos = value->start;
	
	if ( next(value) == '-' )
	{
		negative = true;
		if ( !skip(value) )
        {
            return json_err_invalid;
        }
	}
	
	while ( digit(next(value)) )
	{
		v = v * 10 + (next(value) - '0');
		if ( !skip(value) )
        {
            return json_err_invalid;
        }
	}
	
	if ( negative )
	{
		v = -v;
	}
	
	*target = v;
	
	return json_err_ok;
}


json_err_t json_decode_bool(json_token_t *value, bool *target, bool default_value)
{
	*target = default_value;
	
	if ( value->type != json_type_bool )
	{
		return json_err_invalid;
	}
	
	if ( value->source[value->start] == 't' )
	{ // we've already eastablished that the value here is either "true" or "false", in read_bool() while validating
		*target = true;
	} else
	{
		*target = false;
	}
	
	return json_err_ok;
}


int json_strcmp(json_token_t *value, const char *str)
{
	if ( value->type != json_type_string )
	{
		return -1;
	}
	
	int r = -1;
	
	char *decoded = malloc(value->size+1);
	json_err_t res = json_decode_string(value, decoded, value->size+1);
	
	if ( res != json_err_ok )
	{
		goto cleanup;
	}
	
	r = strcmp(decoded, str);
	
cleanup:
	free(decoded);
	
	return r;
}
