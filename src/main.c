//
//  main.c
//  json-reader
//
//  Created by Mikael Eiman on 2012-02-23.
//  Copyright (c) 2012 TLab West Systems AB. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "json-reader.h"

static void print_json_token(json_token_t *token)
{
    switch(token->type)
    {
        case json_type_array: {
            json_token_t curr;
            bool first = true;
            
            printf("[\n");
            json_array_prepare(token);
            while ( json_array_next(token, &curr) == json_err_ok)
            {
                if ( !first )
                {
                    printf(",");
                    first = false;
                }
                
                print_json_token(&curr);
            }
            printf("]\n");
        }   break;
        
        case json_type_bool: {
            bool truth;
            json_decode_bool(token, &truth, false);
            printf("%s\n", truth ? "true" : "false");
        }   break;
            
        case json_type_invalid:
            printf("Invalid\n");
            break;
            
        case json_type_null:
            printf("null\n");
            break;
        
        case json_type_number: {
            int val;
            json_decode_int(token, &val, -1);
            printf("%d\n", val);
        }   break;
            
        case json_type_object:
            printf("<object>\n");
            break;
        
        case json_type_string: {
            size_t bufsize = token->size+1;
            char *s = malloc(bufsize);
            if ( json_decode_string(token, s, bufsize) == json_err_ok )
            {
                printf("\"%s\"\n", s);
            } else
            {
                printf("invalid string\n");
            }
        }   break;
            
    }
}


int main (int argc, const char * argv[])
{
    if ( argc < 3 )
    {
        printf("Usage: json_reader path-to-json-file path-to-value\n");
        exit(1);
    }
    
	const char *path = argv[1];
	
	printf("Opening %s\n", path);
	
	FILE* f = fopen(path, "r");
	
    fseek(f, 0, SEEK_END);
	size_t file_size = ftell(f);
    fseek(f, 0, SEEK_SET);
    
	uint8_t *blob = malloc(file_size+1);
    size_t file_pos = 0;
    
	while ( !feof(f) ) {
		size_t num_read = fread(&blob[file_pos], 1, file_size-file_pos, f);
		file_size += num_read;
	}
	
	blob[file_size] = 0;
	fclose(f);
	
	printf("Processing...\n");
	
	json_token_t token;
	json_err_t res = json_validate((const char*)blob, &token);
	
	printf("Result: %d\n", res);
	
	json_token_t value;
	res = json_find(&token, argv[2], &value);
	
	printf("Find result: %d\n", res);
	
    print_json_token(&value);
    
    printf("Done\n");
    
    return 0;
}

