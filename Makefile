GTEST_DIR =gtest-1.7.0

TEST_SOURCES = tests/*.cpp
LIB_SOURCES = src/json-reader.c
LIB_HEADERS = src/json-reader.h

CFLAGS = -std=c99
CPPFLAGS = -I$(GTEST_DIR)/include -DGTEST_HAS_PTHREAD=0
CXXFLAGS += -g -Wall -Wextra -DGTEST_HAS_PTHREAD=0

# All Google Test headers.  Usually you shouldn't change this
# definition.
GTEST_HEADERS = $(GTEST_DIR)/include/gtest/*.h \
                $(GTEST_DIR)/include/gtest/internal/*.h


all: test_runner


# Builds gtest.a and gtest_main.a.

# Usually you shouldn't tweak such internal variables, indicated by a
# trailing _.
GTEST_SRCS_ = $(GTEST_DIR)/src/*.cc $(GTEST_DIR)/src/*.h $(GTEST_HEADERS)

# For simplicity and to avoid depending on Google Test's
# implementation details, the dependencies specified below are
# conservative and not optimized.  This is fine as Google Test
# compiles fast and for ordinary users its source rarely changes.
build/gtest-all.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c \
            $(GTEST_DIR)/src/gtest-all.cc -o build/gtest-all.o

build/gtest_main.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c \
            $(GTEST_DIR)/src/gtest_main.cc -o build/gtest_main.o

build/gtest.a : build/gtest-all.o
	$(AR) $(ARFLAGS) $@ $^

build/gtest_main.a : build/gtest-all.o build/gtest_main.o
	$(AR) $(ARFLAGS) $@ $^


build : 
	mkdir -p build

# Tests!

build/json-reader.o: build $(LIB_SOURCES)
	$(CC) -c $(CFLAGS) $(LIB_SOURCES) -o build/json-reader.o


test_runner: build $(TEST_SOURCES) $(HEADERS) build/json-reader.o build/gtest_main.a
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR)/include $(TEST_SOURCES) build/gtest_main.a build/json-reader.o -o test_runner
	./test_runner


tests: test_runner Makefile
	./test_runner


clean:
	rm -f *.a *.o test_runner build/*
